#!/bin/bash
echo "Choix 1: Supprimer un groupe "
echo "Choix 2: Créer un groupe "

read -p "Quel est votre choix ? : " choix
read -p "Quel est le nom du groupe ? : " groupe

if grep -q $groupe /etc/group
then
  si_groupe_existe=true
else
  si_groupe_existe=false
fi

case $choix in

   1)
    if [ "$si_groupe_existe" = true ]
    then
      echo "Le groupe est supprimé"
      sudo groupdel $groupe
    else
      echo "Le groupe n'existe pas"
    fi
    ;;
   2)
    if [ "$si_groupe_existe" = false ]
    then
      echo "Groupe crée"
      sudo groupadd $groupe
    else
      echo "Le groupe existe déjà"
    fi
    ;;
esac

