#!/bin/bash
echo "Choix 1: Supprimer un utilisateur "
echo "Choix 2: Créer un utilisateur "

read -p "Quel est votre choix ? : " choix
read -p "Quel utilisateur ? : " utilisateur

if id "$utilisateur" &>/dev/null;
then
  si_utilisateur_existe=true
else
  si_utilisateur_existe=false
fi

case $choix in
  1)
    if [ "$si_utilisateur_existe" = true ]
    then
      echo "Utilisateur supprimé"
      sudo userdel $utilisateur
    else
      echo "L'utilisateur n'existe pas"
    fi
    ;;
  2)
    if [ "$si_utilisateur_existe" = false ]
    then
      echo "Utilisateur crée"
      sudo useradd $utilisateur
    else
      echo "L'utilisateur existe déjà"
    fi
    ;;
esac

