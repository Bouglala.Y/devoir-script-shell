#!/bin/bash
function poursuivre
{
    echo "Saisissez entrée pour poursuivre"
    read
}

function rentrer_votreinformation
{
    echo "Saisissez votre identifiant "
    read utilisateur
}

function uididentifiant_utilisateur
{
    if grep "^$utilisateur:" /etc/passwd > /dev/null
    then
        echo "L'identifiant existe"
    else
        echo "L'identifiant n'existe pas"
    fi
    poursuivre
}
rep=1
while [ $rep -eq 1 ]
do
    clear
    printf "menu :\n\n"
    echo "Saisissez 1 pour vérifier l'existence d'un identifiant"
    echo "Saisissez 2 pour connaitre l'identifiant utilisateur"
    echo -e "saisir la lettre q pour  Quitter\n"
    read reponse
    case $reponse in
        1)
            rentrer_votreinformation
            uididentifiant_utilisateur ;;
       2)
            rentrer_votreinformation
            id $utilisateur
            poursuivre ;;

        q)
            echo "Bravo et merci a vous d'avoir saisi ces informations"
            poursuivre
            rep=0 ;;
        *)
            echo "Il y a une erreur veuillez s'il vous plait recommencer "
                        poursuivre ;;
    esac
done

