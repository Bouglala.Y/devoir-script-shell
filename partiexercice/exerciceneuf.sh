#!/bin/bash
factorial () {
if [ $1 -eq 1 ] 
then
    echo 1
return
else
    echo $(( $( factorial $(($1 - 1)) ) * $1 ))
fi
}
factorial $1

